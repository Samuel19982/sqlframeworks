package de.wa.samuel.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtils {
	
	private static final String NAME_OF_PERSISTANCE_UNIT = 
			"hibernate-persistence";
	
	private static EntityManagerFactory entityManagerFactory;
	
	private EntityManagerUtils() {
	}

	/**
	 * Erzeugt einen EntityManager und gibt ihn zurueck, da bei {@link #entityManagerFactory} das Singleton Muster
	 * angewandt wird, wird zuerst ueberprueft, ob dieser {@code null} ist.
	 * @return der erzeugte EntityManager
	 */
	public static EntityManager createEntityManager() {
		if (entityManagerFactory == null) {
			entityManagerFactory = Persistence.createEntityManagerFactory(NAME_OF_PERSISTANCE_UNIT);
		}
		
		return entityManagerFactory.createEntityManager();
	}

	/**
	 * Schliesst den EntityManager
	 * @param entityManager der zu schliessende EntityManager
	 */
	public static void closeEntityManager(final EntityManager entityManager) {
		entityManager.close();
	}

}
