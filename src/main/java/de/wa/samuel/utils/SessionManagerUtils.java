package de.wa.samuel.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SessionManagerUtils {
	private static final String NAME_OF_PERSISTANCE_UNIT =
			"hibernate-persistence";

	private static SessionFactory sessionFactory;

	private SessionManagerUtils() {
	}

	/**
	 * Erzeugt einen SessionManager und gibt ihn zurueck, da bei {@link #sessionFactory} das Singleton Muster
	 * angewandt wird, wird zuerst ueberprueft, ob dieser {@code null} ist.
	 * @return der erzeugte EntityManager
	 */
	public static EntityManager createSessionManager() {
		if (sessionFactory == null) {
			sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		}

		return sessionFactory.createEntityManager();
	}

	/**
	 * Schliesst den EntityManager
	 * @param entityManager der zu schliessende EntityManager
	 */
	public static void closeEntityManager(final EntityManager entityManager) {
		entityManager.close();
	}


}
