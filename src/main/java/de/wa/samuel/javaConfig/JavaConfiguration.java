package de.wa.samuel.javaConfig;

import de.wa.samuel.dao.classes.hibernate.PersonDaoHibernateImpl;
import de.wa.samuel.dao.classes.jdbc.PersonDaoJDBCImpl;
import de.wa.samuel.dao.classes.jpa.PersonDaoJPAImpl;
import de.wa.samuel.dao.interfaces.PersonDao;
import de.wa.samuel.dao.interfaces.PersonDaoHibernate;
import de.wa.samuel.dao.interfaces.PersonDaoJPA;
import de.wa.samuel.service.classes.PersonServiceHibernateImpl;
import de.wa.samuel.service.classes.PersonServiceJPAImpl;
import de.wa.samuel.service.classes.PersonServiceImpl;
import de.wa.samuel.service.interfaces.PersonService;
import de.wa.samuel.service.interfaces.PersonServiceHibernate;
import de.wa.samuel.service.interfaces.PersonServiceJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;

@Configuration
public class JavaConfiguration {

	@Autowired
	private PersonDao personDao;

	@Autowired
	private PersonDaoJPA personDaoJPA;

	@Autowired
	private PersonDaoHibernate personDaoHibernate;

	@Bean
	public PersonService personService() {
		return new PersonServiceImpl(personDao);
	}

	@Bean
	public PersonServiceJPA personServiceJPA()
	{
		return new PersonServiceJPAImpl(personDaoJPA);
	}

	@Bean
	public PersonDao personDao(){
		return new PersonDaoJDBCImpl();
	}

	@Bean
	public PersonDaoJPA personDaoJPA(){
		return new PersonDaoJPAImpl();
	}

	@Bean
	public PersonServiceHibernate personServiceHibernate(){
		return new PersonServiceHibernateImpl(personDaoHibernate);
	}

	@Bean
	public PersonDaoHibernate personDaoHibernate(){
		return new PersonDaoHibernateImpl();
	}
}
