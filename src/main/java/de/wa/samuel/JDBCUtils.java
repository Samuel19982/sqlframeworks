package de.wa.samuel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCUtils {
    private static final  String URL="jdbc:sqlite:Peoples1.sqlite";
    private static final String USER="";
    private static final String PASSWORD="";



    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL,USER,PASSWORD);
    }

    public static boolean closeConnection(Connection connection){
        try {
            connection.close();
            return connection.isClosed();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
