package de.wa.samuel.model;



/**
 * Eine Personenklasse, die die Attribute Name und Vorname beinhaltet.
 * Die Id wird zum Abspeichern in der Datenbank benoetigt.
 */
public class PersonImpl implements Person {

    private long id;
    private String name;
    private String firstName;

    protected PersonImpl(){

    }

    public PersonImpl(long id,String name, String vorname) {
        this.id=id;
        this.firstName=vorname;
        this.name=name;
    }

    public PersonImpl(String name, String vorname) {
        this.firstName=vorname;
        this.name=name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                '}';
    }
}