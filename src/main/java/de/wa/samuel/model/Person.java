package de.wa.samuel.model;

/**
 * Eine Personenklasse, die die Attribute Name und Vorname beinhaltet.
 * Die Id wird zum Abspeichern in der Datenbank benoetigt.
 */
public interface Person {
	/**
	 * Gibt den Nachnamen zurueck
	 * @return der Nachname
	 */
	String getName();

	/**
	 * Gibt den Vornamen zurueck
	 * @return der Vorname
	 */
	String getFirstName();

	/**
	 * Gibt die Id zurueck
	 * @return die Id
	 */
	long getId();

	/**
	 * Setzt die Id der Person
	 * @param id die zusetzende Id
	 */
	void setId(long id);

}