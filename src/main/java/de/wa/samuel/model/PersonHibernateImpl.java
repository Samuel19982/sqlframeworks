package de.wa.samuel.model;

import javax.persistence.*;

@Entity
@Table(name="PersonJPA")
public class PersonHibernateImpl implements Person{
	private long id;
	private String name;
	private String firstName;

	protected PersonHibernateImpl(){

	}

	public PersonHibernateImpl(long id,String name, String vorname) {
		this.id=id;
		this.firstName=vorname;
		this.name=name;
	}

	public PersonHibernateImpl(String name, String vorname) {
		this.firstName=vorname;
		this.name=name;
	}

	@Column(name = "name")
	@Override
	public String getName() {
		return name;
	}

	@Column(name="firstName")
	@Override
	public String getFirstName() {
		return firstName;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Person{" +
				"id=" + id +
				", name='" + name + '\'' +
				", firstName='" + firstName + '\'' +
				'}';
	}
}
