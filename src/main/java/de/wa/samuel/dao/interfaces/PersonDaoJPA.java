package de.wa.samuel.dao.interfaces;

import de.wa.samuel.model.Person;

public interface PersonDaoJPA extends PersonDao {

	boolean deletePerson(Person person);
}
