package de.wa.samuel.dao.interfaces;

import de.wa.samuel.model.Person;

import java.util.List;

public interface PersonDao {

	/**
	 * Fuegt eine Person ein
	 * @param person  die einzufuegende Person
	 */
	void insertPerson(Person person);

	/**
	 * Findet alle Personen
	 * @return eine Liste aller Personen
	 */
	List<Person> findAllPeople();

	/**
	 * Findet alle Personen nach der Id
	 * @param id die Id
	 * @return die Person passend zur Id
	 */
	Person findPersonById(long id);

	/**
	 * Loescht eine Person anhand seiner Id
	 * @param id die Id der zu löschenden Person
	 * @return eine boolsche Variable, ob die Operation erfolgreich war
	 */
	boolean deletePerson(long id);

	/**
	 * Loescht eine Person anhand seiner Name und seinem Vornamen
	 * @param name der Name
	 * @param firstName der Vorname
	 * @return eine boolsche Variable, ob die Operation erfolgreich war
	 */
	boolean deletePersonByNameAndFirstName(String name, String firstName);

	/**
	 * Zaehlt die eingefuegten Personen und gibt die Zahl zurueck
	 * @return die Anzahl an Personen in der Datenbank
	 */
	long countPeople();


}
