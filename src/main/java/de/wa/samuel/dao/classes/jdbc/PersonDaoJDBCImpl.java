package de.wa.samuel.dao.classes.jdbc;

import de.wa.samuel.JDBCUtils;
import de.wa.samuel.dao.interfaces.PersonDao;
import de.wa.samuel.model.Person;
import de.wa.samuel.model.PersonImpl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Die PersonDaoImpl sie arbeitet direkt mit der Datenbank
 */
public class PersonDaoJDBCImpl implements PersonDao {


	private static final String COL_PERSON_ID = "id";
	private static final String COL_PERSON_NAME = "name";
	private static final String COL_PERSON_FIRSTNAME = "firstName";
	private static final String COL_COUNT = "PersonCount";


	public PersonDaoJDBCImpl() {
	}

	/**
	 * Fuegt eine Person ein
	 * @param person  die einzufuegende Person
	 */
	@Override
	public void insertPerson(Person person) {
		Connection connection = null;
		try {
			person.setId(countPeople() + 1);
			connection = JDBCUtils.getConnection();
			final Statement statement = connection.createStatement();
			final String queryString = "INSERT INTO People VALUES" +
					" ("+ person.getId()+",'"+person.getName()+"','"+person.getFirstName()+"')";
			statement.executeUpdate(queryString);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}

		}
	}

	/**
	 * Findet alle Personen
	 * @return eine Liste aller Personen
	 */
	@Override
	public List<Person> findAllPeople() {
		Connection connection = null;
		final List<Person> personArrayList = new ArrayList<>();
		try {
			connection = JDBCUtils.getConnection();
			final Statement statement = connection.
					createStatement();
			final String queryString = "SELECT * FROM People";
			final ResultSet resultSet = statement.
					executeQuery(queryString);

			while (resultSet.next()) {
				final Person person = createPersonFromResultSet(resultSet);
				personArrayList.add(person);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}

		}
		return personArrayList;
	}

	/**
	 * Findet alle Personen nach der Id
	 * @param id die Id
	 * @return die Person passend zur Id
	 */
	@Override
	public Person findPersonById(long id) {
		Connection connection = null;
		try {
			connection = JDBCUtils.getConnection();
			final Statement statement = connection.createStatement();
			final String queryString = "SELECT * from People WHERE "
					+ "id = '" + id
					+ "'";
			final ResultSet resultSet = statement.executeQuery(queryString);
			resultSet.next();
			return createPersonFromResultSet(resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	/**
	 * Loescht eine Person anhand seiner Id
	 * @param id die Id der zu löschenden Person
	 * @return eine boolsche Variable, ob die Operation erfolgreich war
	 */
	@Override
	public boolean deletePerson(long id) {
		Connection connection = null;
		try {
			connection = JDBCUtils.getConnection();
			final Statement statement = connection.createStatement();
			final String queryString = "DELETE from People WHERE "
					+ "id = " + id;
			statement.executeUpdate(queryString);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}

		}
		return false;

	}

	/**
	 * Loescht eine Person anhand seiner Name und seinem Vornamen
	 * @param name der Name
	 * @param firstName der Vorname
	 * @return eine boolsche Variable, ob die Operation erfolgreich war
	 */
	@Override
	public boolean deletePersonByNameAndFirstName(String name, String firstName) {
		Connection connection = null;
		try {
			connection = JDBCUtils.getConnection();
			final Statement statement = connection.createStatement();
			final String queryString = "SELECT * FROM People WHERE firstName='"+
					firstName+"' AND name='"+name+"'";
			ResultSet resultSet=statement.executeQuery(queryString);
			return deletePerson(createPersonFromResultSet(resultSet).getId());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}

		}
		return false;
	}

	public long createTable() {
		Connection connection = null;
		try {
			connection = JDBCUtils.getConnection();
			final Statement statement = connection.createStatement();
			final String queryString = "SELECT COUNT(*) as "
					+ COL_COUNT	+ " FROM People";
			final ResultSet resultSet = statement.executeQuery(queryString);
			resultSet.next();
			return resultSet.getInt(COL_COUNT);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return 0;
	}

	/**
	 * Zaehlt die eingefuegten Personen und gibt die Zahl zurueck
	 * @return die Anzahl an Personen in der Datenbank
	 */
	@Override
	public long countPeople() {
		Connection connection = null;

		try {
			connection = JDBCUtils.getConnection();
			final Statement statement = connection.createStatement();
			final String queryString = "SELECT COUNT(*) as "
					+ COL_COUNT	+ " FROM People";
			final ResultSet resultSet = statement.executeQuery(queryString);
			resultSet.next();
			return resultSet.getInt(COL_COUNT);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch(SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		return 0;
	}


	/**
	 * Erstellt eine Person anhand des uebergebenen ResultSets
	 * @param resultSet das ResultSet
	 * @return die Person, die in dem ResultSet ist
	 * @throws SQLException eine SQLException, wenn der Schluessel nicht gefunden wird.
	 * 5 Zeilen
	 */
	private Person createPersonFromResultSet(final ResultSet resultSet) throws SQLException {
		final long personId = resultSet.getLong(COL_PERSON_ID);
		final String personName = resultSet.getString(COL_PERSON_NAME);
		final String personFirstName = resultSet.getString(COL_PERSON_FIRSTNAME);

		if (resultSet.wasNull()) {
			System.out.println("Bei Person mit der Id " + personId +
					" ist kein Name angegeben!");
		}
		return new PersonImpl(personId, personName, personFirstName);
	}
}
