package de.wa.samuel.dao.classes.jpa;

import de.wa.samuel.dao.interfaces.PersonDaoJPA;
import de.wa.samuel.model.Person;
import de.wa.samuel.model.PersonHibernateImpl;
import de.wa.samuel.utils.EntityManagerUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Objects;

public class PersonDaoJPAImpl implements PersonDaoJPA
{
	@Override
	public void insertPerson(Person person) {
		EntityManager entityManager = null;

		try {
			entityManager = EntityManagerUtils.createEntityManager();

			entityManager.getTransaction().begin();
			entityManager.persist(person);
			entityManager.getTransaction().commit();
		} finally {
			EntityManagerUtils.closeEntityManager(Objects.requireNonNull(entityManager));
		}
	}

	@Override
	public List<Person> findAllPeople() {
		List<Person> resultList;
		final String queryString = "SELECT c FROM PersonHibernateImpl c";
		EntityManager entityManager = null;

		try {
			entityManager = EntityManagerUtils.createEntityManager();
			entityManager.getTransaction().begin();

			final TypedQuery<Person> query = entityManager.createQuery(queryString, Person.class);
			resultList = query.getResultList();
			entityManager.getTransaction().commit();
			return resultList;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			EntityManagerUtils.closeEntityManager(Objects.requireNonNull(entityManager));
		}
	}

	@Override
	public Person findPersonById(long id) {
		EntityManager entityManager = null;
		try {
			entityManager = EntityManagerUtils.createEntityManager();
			entityManager.getTransaction().begin();
			Person person = entityManager.find(
					PersonHibernateImpl.class, id);
			entityManager.getTransaction().commit();
			return person;
		}
		finally {
			if (entityManager!=null) {
				EntityManagerUtils.closeEntityManager(entityManager);
			}
		}
	}

	@Override
	public boolean deletePerson(long id) {
		final Person person = findPersonById(id);
		return deletePerson(person);
	}

	@Override
	public boolean deletePerson(Person person) {
		EntityManager entityManager = null;

		try {
			entityManager = EntityManagerUtils.createEntityManager();
			entityManager.getTransaction().begin();

			final Person personToDelete = entityManager.find(
					PersonHibernateImpl.class, person.getId());
			if (personToDelete != null) {
				entityManager.remove(personToDelete);
			}
			entityManager.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			EntityManagerUtils.closeEntityManager(Objects.requireNonNull(entityManager));
		}

		return false;
	}

	@Override
	public boolean deletePersonByNameAndFirstName(String name, String firstName) {

		EntityManager entityManager = null;
		final String queryString = "SELECT c FROM PersonHibernateImpl c WHERE c.name="+name+" AND c.firstName="+firstName;

		try {

			entityManager = EntityManagerUtils.createEntityManager();
			entityManager.getTransaction().begin();

			final TypedQuery<Person> query = entityManager.createQuery(queryString, Person.class);
			Person personToDelete=query.getSingleResult();

			if (personToDelete != null) {
				entityManager.remove(personToDelete);
			}
			entityManager.getTransaction().commit();
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			EntityManagerUtils.closeEntityManager(Objects.requireNonNull(entityManager));
		}
		return false;


	}

	@Override
	public long countPeople() {

		EntityManager entityManager = null;

		try {
			entityManager = EntityManagerUtils.createEntityManager();
			entityManager.getTransaction().begin();

			final String queryString = "SELECT count(c) FROM PersonHibernateImpl c";
			final Query query = entityManager.createQuery(queryString);
			final long personCount = (long)query.getSingleResult();
			entityManager.getTransaction().commit();
			return personCount;
		}
		catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		finally {
			if (entityManager != null) {
				EntityManagerUtils.closeEntityManager(entityManager);
			}
		}
	}
}
