package de.wa.samuel.service.classes;

import de.wa.samuel.dao.interfaces.PersonDaoJPA;
import de.wa.samuel.model.Person;
import de.wa.samuel.service.interfaces.PersonServiceJPA;

import java.util.List;

public class PersonServiceJPAImpl implements PersonServiceJPA {

	private final PersonDaoJPA personDao;

	public PersonServiceJPAImpl(PersonDaoJPA personDao) {
		this.personDao=personDao;
	}

	@Override
	public void insertPerson(Person person) {
		personDao.insertPerson(person);
	}

	@Override
	public List<Person> findAllPeople() {
		return personDao.findAllPeople();
	}

	@Override
	public Person findPersonById(int id) {
		return personDao.findPersonById(id);
	}

	@Override
	public boolean deletePerson(int id) {
		return personDao.deletePerson(id);
	}

	@Override
	public boolean deletePersonByNameAndFirstName(String name, String firstName) {
		return personDao.deletePersonByNameAndFirstName(name, firstName);
	}

	@Override
	public long countPeople() {
		return personDao.countPeople();
	}

	@Override
	public boolean deletePerson(Person person) {
		return false;
	}
}
