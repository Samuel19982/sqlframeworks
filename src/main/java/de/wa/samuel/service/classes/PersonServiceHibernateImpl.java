package de.wa.samuel.service.classes;

import de.wa.samuel.dao.interfaces.PersonDaoHibernate;
import de.wa.samuel.model.Person;
import de.wa.samuel.service.interfaces.PersonServiceHibernate;

import java.util.List;

public class PersonServiceHibernateImpl implements PersonServiceHibernate {

	private final PersonDaoHibernate personDao;

	public PersonServiceHibernateImpl(PersonDaoHibernate personDao) {
		this.personDao=personDao;
	}

	@Override
	public void insertPerson(Person person) {
		personDao.insertPerson(person);
	}

	@Override
	public List<Person> findAllPeople() {
		return personDao.findAllPeople();
	}

	@Override
	public Person findPersonById(int id) {
		return personDao.findPersonById(id);
	}

	@Override
	public boolean deletePerson(int id) {
		return personDao.deletePerson(id);
	}

	@Override
	public boolean deletePersonByNameAndFirstName(String name, String firstName) {
		return personDao.deletePersonByNameAndFirstName(name, firstName);
	}

	@Override
	public long countPeople() {
		return personDao.countPeople();
	}

	@Override
	public boolean deletePerson(Person person) {
		return false;
	}
}


