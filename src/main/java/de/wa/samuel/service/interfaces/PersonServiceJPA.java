package de.wa.samuel.service.interfaces;

import de.wa.samuel.model.Person;

public interface PersonServiceJPA extends PersonService{
	boolean deletePerson(Person person);

}
