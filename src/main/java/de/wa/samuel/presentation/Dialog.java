package de.wa.samuel.presentation;

import de.wa.samuel.model.Person;
import de.wa.samuel.model.PersonHibernateImpl;
import de.wa.samuel.model.PersonImpl;
import de.wa.samuel.service.interfaces.PersonService;
import de.wa.samuel.service.interfaces.PersonServiceHibernate;
import de.wa.samuel.service.interfaces.PersonServiceJPA;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

@ShellComponent
public class Dialog {
	private final Scanner sc = new Scanner(System.in);

	private final PersonService personService;
	private final PersonServiceJPA personServiceJPA;
	private final PersonServiceHibernate personServiceHibernate;

	public Dialog(PersonService personService, PersonServiceJPA personServiceJPA, PersonServiceHibernate personServiceHibernate) {
		this.personService = personService;
		this.personServiceJPA = personServiceJPA;
		this.personServiceHibernate = personServiceHibernate;
	}

	@ShellComponent
	@ShellCommandGroup("JDBC Commands")
	public class JDBCCommands {
		@ShellMethod("Füge eine Person ein")
		public void insertPerson() {
			System.out.print("Nachname: ");
			String name = sc.nextLine();
			System.out.print("Vorname: ");
			String firstName = sc.nextLine();
			personService.insertPerson(new PersonImpl(name, firstName));
		}

		@ShellMethod("Gebe Personen aus")
		public void printPpl() {
			personService.findAllPeople().forEach(System.out::println);
		}

		@ShellMethod("Lösche eine Person nach ID")
		public void deletePersonByID() {
			System.out.print("Id: ");
			String id = sc.nextLine();
			personService.deletePerson(Integer.parseInt(id));
		}

		@ShellMethod("Lösche eine Person nach Name und FirstName")
		public void deletePersonByNameAndFirstName() {
			System.out.print("Name ");
			String name = sc.nextLine();
			System.out.print("Vorname: ");
			String firstName = sc.nextLine();
			personService.deletePersonByNameAndFirstName(name, firstName);
		}

		@ShellMethod("Finde eine Person nach Id")
		public void findPersonByID() {
			System.out.print("Die Id zur suchenden Person ");
			String id = sc.nextLine();
			Person person = personService.findPersonById(Integer.parseInt(id));
			System.out.println(person);
		}
	}

	@ShellComponent
	@ShellCommandGroup("JPA Commands")
	public class JPACommands {
		@ShellMethod("Füge eine Person ein")
		public void insertPersonH() {
			System.out.print("Nachname: ");
			String name = sc.nextLine();
			System.out.print("Vorname: ");
			String firstName = sc.nextLine();
			personServiceJPA.insertPerson(new PersonHibernateImpl(name, firstName));
		}

		@ShellMethod("Gebe Personen aus")
		public void printPplH() {
			personServiceJPA.findAllPeople().forEach(System.out::println);
		}

		@ShellMethod("Lösche eine Person nach ID")
		public void deletePersonByIDH() {
			System.out.print("Id: ");
			String id = sc.nextLine();
			personServiceJPA.deletePerson(Integer.parseInt(id));
		}

		@ShellMethod("Lösche eine Person nach Name und FirstName")
		public void deletePersonByNameAndFirstNameH() {
			System.out.print("Name ");
			String name = sc.nextLine();
			System.out.print("Vorname: ");
			String firstName = sc.nextLine();
			personServiceJPA.deletePersonByNameAndFirstName(name, firstName);
		}

		@ShellMethod("Finde eine Person nach Id")
		public void findPersonByIDH() {
			System.out.print("Die Id zur suchenden Person ");
			String id = sc.nextLine();
			Person person = personServiceJPA.findPersonById(Integer.parseInt(id));
			System.out.println(person);
		}
	}


	@ShellComponent
	@ShellCommandGroup("Hibernate Commands")
	public class HibernateCommands {
		@ShellMethod("Füge eine Person ein")
		public void insertPersonHib() {
			System.out.print("Nachname: ");
			String name = sc.nextLine();
			System.out.print("Vorname: ");
			String firstName = sc.nextLine();
			personServiceHibernate.insertPerson(new PersonImpl(name, firstName));
		}

		@ShellMethod("Gebe Personen aus")
		public void printPplHib() {
			personServiceHibernate.findAllPeople().forEach(System.out::println);
		}

		@ShellMethod("Lösche eine Person nach ID")
		public void deletePersonByIDHib() {
			System.out.print("Id: ");
			String id = sc.nextLine();
			personServiceHibernate.deletePerson(Integer.parseInt(id));
		}

		@ShellMethod("Lösche eine Person nach Name und FirstName")
		public void deletePersonByNameAndFirstNameHib() {
			System.out.print("Name ");
			String name = sc.nextLine();
			System.out.print("Vorname: ");
			String firstName = sc.nextLine();
			personServiceHibernate.deletePersonByNameAndFirstName(name, firstName);
		}

		@ShellMethod("Finde eine Person nach Id")
		public void findPersonByIDHib() {
			System.out.print("Die Id zur suchenden Person ");
			String id = sc.nextLine();
			Person person = personServiceHibernate.findPersonById(Integer.parseInt(id));
			System.out.println(person);
		}
	}


	@ShellComponent
	@ShellCommandGroup("Benchmark Commands")
	public class Benchmarker {

		@ShellMethod("Benchmark With 100 people")
		public void benchmarkWith100People(){
			List<PersonImpl> peopleListForJDBC=new LinkedList<>();

			List<Person> peopleListForJPA=new LinkedList<>();

			List<Person> peopleListForHibernate=new LinkedList<>();


			IntStream.range(0, 100).forEach(i -> peopleListForJDBC.add(
					new PersonImpl("Name %s".formatted(i),
					"Nachname %s".formatted(i))));

			IntStream.range(0, 100).forEach(i -> peopleListForJPA.add(
					new PersonHibernateImpl("Name %s".formatted(i),
							"Nachname %s".formatted(i))));

			IntStream.range(0, 100).forEach(i -> peopleListForHibernate.add(
					new PersonImpl("Name %s".formatted(i),
							"Nachname %s".formatted(i))));

			addPeopleToJDBC(peopleListForJDBC);

			addPeopleToJPA(peopleListForJPA);
			addPeopleToHibernate(peopleListForHibernate);

		}

		private void addPeopleToJDBC(List<PersonImpl> personList){
			System.out.println("JDBC Test");
			long beforeJDBCStart=System.nanoTime();
			personList.forEach(personService::insertPerson);
			long afterJDBCStart=System.nanoTime();
			printPassedTime(beforeJDBCStart,afterJDBCStart);
		}


		private void addPeopleToJPA(List<Person> personList){
			System.out.println("JPA Test");
			long beforeJPAStart=System.nanoTime();
			personList.forEach(personServiceJPA::insertPerson);
			long afterJPAStart=System.nanoTime();
			printPassedTime(beforeJPAStart,afterJPAStart);
		}

		private void addPeopleToHibernate(List<Person> personList){
			System.out.println("Hibernate Test");
			long beforeHibernateStart=System.nanoTime();
			personList.forEach(personServiceHibernate::insertPerson);
			long afterHibernateStart=System.nanoTime();
			printPassedTime(beforeHibernateStart,afterHibernateStart);
		}

		public void printPassedTime(long start, long end){
			System.out.println("Vergangene Nanosekunden: "+Math.abs(end-start));
			System.out.println("Vergangene Millisekunden: "+(Math.abs(end-start)/1000000));
			System.out.println("\n\n");
		}


		@ShellMethod("Benchmark With 1000 people")
		public void benchmarkWith1000People(){
			List<PersonImpl> peopleListForJDBC=new LinkedList<>();

			List<Person> peopleListForJPA=new LinkedList<>();

			List<Person> peopleListForHibernate=new LinkedList<>();


			IntStream.range(0, 1000).forEach(i -> peopleListForJDBC.add(
					new PersonImpl("Name %s".formatted(i),
							"Nachname %s".formatted(i))));

			IntStream.range(0, 1000).forEach(i -> peopleListForJPA.add(
					new PersonHibernateImpl("Name %s".formatted(i),
							"Nachname %s".formatted(i))));

			IntStream.range(0, 1000).forEach(i -> peopleListForHibernate.add(
					new PersonImpl("Name %s".formatted(i),
							"Nachname %s".formatted(i))));

			addPeopleToJDBC(peopleListForJDBC);

			addPeopleToJPA(peopleListForJPA);
			addPeopleToHibernate(peopleListForHibernate);
		}

		@ShellMethod("Benchmark With 10000 people")
		public void benchmarkWith10000People(){
			List<PersonImpl> peopleListForJDBC=new LinkedList<>();

			List<Person> peopleListForJPA=new LinkedList<>();

			List<Person> peopleListForHibernate=new LinkedList<>();


			IntStream.range(0, 10000).forEach(i -> peopleListForJDBC.add(
					new PersonImpl("Name %s".formatted(i),
							"Nachname %s".formatted(i))));

			IntStream.range(0, 10000).forEach(i -> peopleListForJPA.add(
					new PersonHibernateImpl("Name %s".formatted(i),
							"Nachname %s".formatted(i))));

			IntStream.range(0, 10000).forEach(i -> peopleListForHibernate.add(
					new PersonImpl("Name %s".formatted(i),
							"Nachname %s".formatted(i))));

			addPeopleToJDBC(peopleListForJDBC);

			addPeopleToJPA(peopleListForJPA);
			addPeopleToHibernate(peopleListForHibernate);
		}
	}

}
